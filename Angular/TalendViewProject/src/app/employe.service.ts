import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class EmployeService {

 
  url="http://localhost:8088/";

 
  
  constructor(private http : HttpClient) {   }


  getAllUsersByblos(): Observable<any>{
    return this.http.get(this.url+"usersByblos")
  }


  getAllUsersCovoiturage(): Observable<any>{
    return this.http.get(this.url+"usersCovoiturage")  
  }

  getAllReservationCovoiturage() :Observable<any>{
    return this.http.get(this.url+"reservations")  
  }


  sendMailTo( mail :String) :Observable<any>{ 
    const  options= {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    } 
    return this.http.get(this.url+"sendMail/"+mail,options)  
  }

  deleteReservation(id:number):Observable<any>{
    const  options= {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    } 
    return this.http.get(this.url+"reservation/"+id,options)  
  }

  createUser(id: number, login_user : String , nom_user :String ,prenom_user : String , email :String): Observable<Object> {
      const  options= {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin': '*'
        })
      } 
    return this.http.post(this.url+id+"/"+login_user+"/"+nom_user+"/"+prenom_user+"/"+email,null,options);
  }


  
  deleteUser(id: number): Observable<any> {
    const  options= {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    } 
  return this.http.get(this.url+"users/"+id,options);
}


  updateAlldb():Observable <any> {
    return this.http.get(this.url+"updateAll");
  }
}
