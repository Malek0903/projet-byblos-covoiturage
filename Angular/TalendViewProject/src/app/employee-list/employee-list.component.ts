import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeService } from '../employe.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppComponent } from '../app.component';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})

export class EmployeeListComponent implements OnInit {
  newListByblos: any[] = [];
  newListCovoiturage: any[] = [];
  newListtoBeUpdated: any[] = [];
  newListByblosBackup: any[] = [];
  employeListLoadin = true;
  searchWord: string;
  successAdd = 0;
  nbElementToAdd: number;
  newUsersToBeAded: any[] = [];
  newUsersToBeDeleted: any[] = [];
  newUsersListToBeNotifiedByMail: any[] = [];
  constructor(private employeService: EmployeService,
    private router: Router, private activateRoute: ActivatedRoute,
    public dialog: MatDialog) { }

  ngOnInit(): void {

    this.getAllList();
  }


  updateCheckedOptions(option, event){
    console.log(option, event.target.checked);
    
  }


  public findUserInList(user) {
    return this.newUsersToBeAded.findIndex(u =>
      u.id_user == user.id_user);
  }

  public findUserInListToBeDeleted(user) {
    return this.newUsersToBeDeleted.findIndex(u =>
      u.id == user.id);
  }

  addAndRemoveAllUsersToList(user,options) {
    if (options.target.checked == true)
      this.newUsersToBeAded.push(user);
    else this.newUsersToBeAded.splice(
      this.findUserInList(user), 1
    )
    //console.log(user);
    console.log("new user list ", this.newUsersToBeAded);
  }




  addAndRemoveAllUsersToListToBeDeleted(user,options) {
    if (options.target.checked == true)
      this.newUsersToBeDeleted.push(user);
    else this.newUsersToBeDeleted.splice(
      this.findUserInList(user), 1
    )
    // console.log(user);
    //console.log("new user list " , this.newUsersToBeDeleted);
  }



  addAllUsers() {
    if (confirm("Are you sure to add all users selected")) {
      for (let index = 0; index < this.newUsersToBeAded.length; index++) {
        //console.log(this.newUsersToBeAded[index].id_user)
        this.createUser(this.newUsersToBeAded[index].id_user,
          this.newUsersToBeAded[index].login_user,
          this.newUsersToBeAded[index].nom_user,
          this.newUsersToBeAded[index].prenom_user);
      }
    }
  }

  deleteAllUsers() {
    

      for (let index = 0; index < this.newUsersToBeDeleted.length; index++) {

        //console.log(this.newUsersToBeDeleted[index].id)
        this.deleteUser(this.newUsersToBeDeleted[index].id);
      
    }
  }

  getAllReservation() {
    this.employeService.getAllReservationCovoiturage()
      .subscribe((res) => {
        console.log(res);
      });
  }

  ///users manage section 
  addUserToCovoiturage(id: number, login_user: String, nom_user: String, prenom_user: String) {
    if (confirm("Are you sure to add " + login_user)) {
      this.createUser(id, login_user, nom_user, prenom_user);

    }
  }

  updateAllDB() {
    this.employeService.updateAlldb().subscribe(
      (reslt) => {
        location.reload();
        // console.log(reslt);

      }
    );

  }

  createUser(id: number, login_user: String, nom_user: String, prenom_user: String) {
    //console.log(id,login_user,nom_user,prenom_user,"malek.benyakhlef@esprit.tn");
    this.employeService.createUser(id, login_user, nom_user, prenom_user, "malekbenyakhlef1@gmail.com")
      .subscribe(
        (respons) => {
          location.reload();
          //console.log(respons)
        },
        (err) => { console.log(err) }
      );
  }

  deleteUser(id: number) {
  if (confirm("Are you sure to delete " )) {
    this.employeService.deleteUser(id)
      .subscribe(
        (ress) => {
          this.getDifferenceBetwenUsersCovoiturageAndReservation();
        /*   location.reload() */
        },
        (err) => { console.log(err) }
      );
  }
  }


  notifyUsers() {
    for (let index = 0; index < this.newUsersListToBeNotifiedByMail.length; index++) {
      this.employeService.sendMailTo(this.newUsersListToBeNotifiedByMail[index].email).subscribe((result) => {
        console.log("success");
       
      })
    }
    this.newUsersListToBeNotifiedByMail = [];
    console.log( this.newUsersListToBeNotifiedByMail)

  }

  getDifferenceBetwenUsersCovoiturageAndReservation() {
    console.log("you are here getDifferenceBetwenUsersCovoiturageAndReservation ")
    this.employeService.getAllReservationCovoiturage().subscribe((resrvation) => {
      this.employeService.getAllUsersCovoiturage().subscribe((usersCovoi) => {
        for (let i = 0; i < resrvation.reservations.reservation.length; i++) {
          usersCovoi.users.user.forEach(element => {
            if (resrvation.reservations.reservation[i].userdriver != element.login_user) {
              console.log("does not match ", resrvation.reservations.reservation[i].userpassenger)
              if (resrvation.reservations.reservation[i].userpassenger == element.login_user) {
                this.newUsersListToBeNotifiedByMail.push(element);
                console.log(this.newUsersListToBeNotifiedByMail)
                this.notifyUsers();
                this.employeService.deleteReservation(resrvation.reservations.reservation[i].id).subscribe(reslt=>{
                  console.log(reslt);
                  location.reload();
                }),(err)=>{
                  location.reload();
                };
              }
            }


          });
        }
      }), (error) => { location.reload(); }
    }), (error) => {location.reload(); }


  }
  /*  end user manage section */
  getAllList() {

    this.employeService.getAllUsersCovoiturage()
      .subscribe(
        (userCo) => {
          this.employeService.getAllUsersByblos()
            .subscribe(
              (userByblos) => {
                var onlyInB = userCo.users.user.filter(this.comparer(userByblos.users.user));
                var onlyInA = userByblos.users.user.filter(this.comparer(userCo.users.user));
                this.newListByblos.push(onlyInB.concat(onlyInA));



                //compare two
                var onlyInB2 = userCo.users.user.filter(this.comparer2(userByblos.users.user));
                var onlyInA2 = userByblos.users.user.filter(this.comparer2(userCo.users.user));
                this.newListCovoiturage.push(onlyInB2.concat(onlyInA2));
                // console.log(this.newListCovoiturage)


                //comapre three
                var onlyInB3 = userCo.users.user.filter(this.comparer3(userByblos.users.user));
                var onlyInA3 = userByblos.users.user.filter(this.comparer3(userCo.users.user));
                this.newListtoBeUpdated.push(onlyInB3.concat(onlyInA3));
                //console.log(this.newListtoBeUpdated)

              },
              (error) => { console.log(error) }
            );
        },
        (error) => { console.log(error) }
      );
  }

  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        return other.id == current.id_user
      }).length == 0;
    }
  }

  comparer2(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        return other.id_user == current.id
      }).length == 0;
    }
  }


  comparer3(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        return current.login_user != other.userdriver
      }).length == 0;
    }
  }



  ///three search methods
  resetSearch() {
    location.reload()
    this.searchWord = ''
  }

  search() {
    //console.log("adzjoazdo")
    for (let i = 0; i < this.newListByblos[0].length; i++) {
      if (this.newListByblos[0][i].login_user === this.searchWord || this.newListByblos[0][i].nom_user === this.searchWord || this.newListByblos[0][i].prenom_user === this.searchWord) {
        let empl = this.newListByblos[0][i];
        this.newListByblos[0] = [];
        this.newListByblos[0].push(empl);
      }
    }
  }
  searchByEnterKey(event) {
    if (event.key === "Enter") {
      // console.log("helloo")
      this.search();
    }

  }
}
