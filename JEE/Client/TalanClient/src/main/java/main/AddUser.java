package main;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.github.javafaker.Faker;

import model.RefUser;
import model.Societe;
import service.ITalan;

public class AddUser {

	public static void main(String[] args) throws NamingException {

		String jndiName = "talanProjet/TalanService!service.ITalan";
		Context context = new InitialContext();

		ITalan talanProxy = (ITalan) context.lookup(jndiName);

		
		Faker faker = new Faker();	
		
		for (int i = 54; i < 80; i++) {
		////alimentation de la table societe////
			
			RefUser user = new RefUser();	
//			Societe societe = new Societe();		
//			societe.setIdSoc(i);
//			societe.setRaisonSociale(faker.commerce().department());
//			talanProxy.saveSociete(societe);
		
			
			//alimentation de la table user
			user.setIdUser(i);
			user.setIdSoc(BigDecimal.TEN);
			user.setNomUser(faker.name().firstName());
			user.setPrenomUser(faker.name().lastName());
			user.setLoginUser(faker.name().firstName());
			user.setDtCreate(new Timestamp(new Date().getDate()));
			user.setDescSrc(faker.hitchhikersGuideToTheGalaxy().quote());
			user.setSecretUser(faker.hitchhikersGuideToTheGalaxy().character());
			talanProxy.saveRefUser(user);
		
		}
	}

}
