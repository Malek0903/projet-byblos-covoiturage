package service;

import javax.ejb.Remote;

import model.RefUser;
import model.Societe;

@Remote
public interface ITalan {

	void saveRefUser(RefUser user);
	void saveSociete(Societe societe);
	
	
}
