package service.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.RefUser;
import model.Societe;
import service.ITalan;

@LocalBean
@Stateless
public class TalanService implements ITalan {

	@PersistenceContext
	EntityManager em ;
	
	
	@Override
	public void saveRefUser(RefUser user) {
		em.persist(user);
		
	}


	@Override
	public void saveSociete(Societe societe) {
		em.persist(societe);
		
	}

}
