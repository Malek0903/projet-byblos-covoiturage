package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the societe database table.
 * 
 */
@Entity
@NamedQuery(name="Societe.findAll", query="SELECT s FROM Societe s")
public class Societe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_soc")
	private long idSoc;

	@Column(name="raison_sociale")
	private String raisonSociale;

	public Societe() {
	}

	public long getIdSoc() {
		return this.idSoc;
	}

	public void setIdSoc(long idSoc) {
		this.idSoc = idSoc;
	}

	public String getRaisonSociale() {
		return this.raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

}