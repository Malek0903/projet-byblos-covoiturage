package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ref_user database table.
 * 
 */
@Entity
@Table(name="ref_user")
@NamedQuery(name="RefUser.findAll", query="SELECT r FROM RefUser r")
public class RefUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_user")
	private long idUser;

	@Column(name="actif_user")
	private BigDecimal actifUser;

	private BigDecimal createur;

	@Column(name="desc_src")
	private String descSrc;

	@Column(name="dt_create")
	private Timestamp dtCreate;

	@Column(name="dt_modif")
	private Timestamp dtModif;

	@Column(name="failed_login_attempts")
	private BigDecimal failedLoginAttempts;

	@Column(name="id_pers")
	private BigDecimal idPers;

	@Column(name="id_soc")
	private BigDecimal idSoc;

	@Column(name="is_support")
	private Boolean isSupport;

	private Timestamp lastlogondate;

	@Column(name="login_user")
	private String loginUser;

	@Column(name="mail_bienvenue_envoye")
	private Boolean mailBienvenueEnvoye;

	@Column(name="nom_user")
	private String nomUser;

	private String password;

	@Column(name="prenom_user")
	private String prenomUser;

	@Column(name="ref_user")
	private BigDecimal refUser;

	@Column(name="secret_user")
	private String secretUser;

	@Column(name="user_agent")
	private String userAgent;

	private BigDecimal version;

	public RefUser() {
	}

	public long getIdUser() {
		return this.idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public BigDecimal getActifUser() {
		return this.actifUser;
	}

	public void setActifUser(BigDecimal actifUser) {
		this.actifUser = actifUser;
	}

	public BigDecimal getCreateur() {
		return this.createur;
	}

	public void setCreateur(BigDecimal createur) {
		this.createur = createur;
	}

	public String getDescSrc() {
		return this.descSrc;
	}

	public void setDescSrc(String descSrc) {
		this.descSrc = descSrc;
	}

	public Timestamp getDtCreate() {
		return this.dtCreate;
	}

	public void setDtCreate(Timestamp dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Timestamp getDtModif() {
		return this.dtModif;
	}

	public void setDtModif(Timestamp dtModif) {
		this.dtModif = dtModif;
	}

	public BigDecimal getFailedLoginAttempts() {
		return this.failedLoginAttempts;
	}

	public void setFailedLoginAttempts(BigDecimal failedLoginAttempts) {
		this.failedLoginAttempts = failedLoginAttempts;
	}

	public BigDecimal getIdPers() {
		return this.idPers;
	}

	public void setIdPers(BigDecimal idPers) {
		this.idPers = idPers;
	}

	public BigDecimal getIdSoc() {
		return this.idSoc;
	}

	public void setIdSoc(BigDecimal idSoc) {
		this.idSoc = idSoc;
	}

	public Boolean getIsSupport() {
		return this.isSupport;
	}

	public void setIsSupport(Boolean isSupport) {
		this.isSupport = isSupport;
	}

	public Timestamp getLastlogondate() {
		return this.lastlogondate;
	}

	public void setLastlogondate(Timestamp lastlogondate) {
		this.lastlogondate = lastlogondate;
	}

	public String getLoginUser() {
		return this.loginUser;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	public Boolean getMailBienvenueEnvoye() {
		return this.mailBienvenueEnvoye;
	}

	public void setMailBienvenueEnvoye(Boolean mailBienvenueEnvoye) {
		this.mailBienvenueEnvoye = mailBienvenueEnvoye;
	}

	public String getNomUser() {
		return this.nomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrenomUser() {
		return this.prenomUser;
	}

	public void setPrenomUser(String prenomUser) {
		this.prenomUser = prenomUser;
	}

	public BigDecimal getRefUser() {
		return this.refUser;
	}

	public void setRefUser(BigDecimal refUser) {
		this.refUser = refUser;
	}

	public String getSecretUser() {
		return this.secretUser;
	}

	public void setSecretUser(String secretUser) {
		this.secretUser = secretUser;
	}

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public BigDecimal getVersion() {
		return this.version;
	}

	public void setVersion(BigDecimal version) {
		this.version = version;
	}

}