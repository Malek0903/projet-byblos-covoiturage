package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the ref_personnel database table.
 * 
 */
@Entity
@Table(name="ref_personnel")
@NamedQuery(name="RefPersonnel.findAll", query="SELECT r FROM RefPersonnel r")
public class RefPersonnel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_pers")
	private long idPers;

	@Column(name="annual_amount")
	private BigDecimal annualAmount;

	@Column(name="background_check")
	private String backgroundCheck;

	@Column(name="code_analytique")
	private String codeAnalytique;

	@Column(name="code_pers")
	private String codePers;

	@Column(name="coefficient_cv")
	private BigDecimal coefficientCv;

	@Column(name="compte_tiers")
	private String compteTiers;

	private BigDecimal createur;

	@Column(name="createur_collab")
	private BigDecimal createurCollab;

	@Temporal(TemporalType.DATE)
	@Column(name="date_stc")
	private Date dateStc;

	@Column(name="departement_naissance")
	private String departementNaissance;

	@Column(name="desc_src")
	private String descSrc;

	@Column(name="disc_pers")
	private String discPers;

	private String disponibilite;

	@Column(name="drug_test")
	private String drugTest;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_anciennete")
	private Date dtAnciennete;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_complation_background")
	private Date dtComplationBackground;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_complation_drug")
	private Date dtComplationDrug;

	@Column(name="dt_create")
	private Timestamp dtCreate;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_disponibilite")
	private Date dtDisponibilite;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_effet_conge")
	private Date dtEffetConge;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_entree_entite")
	private Date dtEntreeEntite;

	@Column(name="dt_modif")
	private Timestamp dtModif;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_naissance_pers")
	private Date dtNaissancePers;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_obtention_dip")
	private Date dtObtentionDip;

	@Column(name="dt_premiere_experience")
	private Timestamp dtPremiereExperience;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_visite_medicale")
	private Date dtVisiteMedicale;

	@Column(name="entite_juridique")
	private BigDecimal entiteJuridique;

	@Column(name="est_force")
	private Boolean estForce;

	@Column(name="exit_mails_sent")
	private Boolean exitMailsSent;

	@Column(name="has_cash_advances")
	private Boolean hasCashAdvances;

	@Column(name="has_pto")
	private Boolean hasPto;

	@Column(name="id_bu_manager_rh")
	private BigDecimal idBuManagerRh;

	@Column(name="id_cand_date_dispo")
	private BigDecimal idCandDateDispo;

	@Column(name="id_candidat_ats")
	private String idCandidatAts;

	@Column(name="id_civ")
	private BigDecimal idCiv;

	@Column(name="id_civilite")
	private BigDecimal idCivilite;

	@Column(name="id_collab_categorie")
	private BigDecimal idCollabCategorie;

	@Column(name="id_cooptant")
	private BigDecimal idCooptant;

	@Column(name="id_etab")
	private BigDecimal idEtab;

	@Column(name="id_fonction")
	private BigDecimal idFonction;

	@Column(name="id_formation")
	private BigDecimal idFormation;

	@Column(name="id_info_contrat")
	private BigDecimal idInfoContrat;

	@Column(name="id_job_ats")
	private String idJobAts;

	@Column(name="id_manager")
	private BigDecimal idManager;

	@Column(name="id_nationalite")
	private BigDecimal idNationalite;

	@Column(name="id_offre")
	private BigDecimal idOffre;

	@Column(name="id_pays_ecole")
	private BigDecimal idPaysEcole;

	@Column(name="id_pers_assurance")
	private BigDecimal idPersAssurance;

	@Column(name="id_pers_source")
	private BigDecimal idPersSource;

	@Column(name="id_profil")
	private BigDecimal idProfil;

	@Column(name="id_profil_ressource")
	private BigDecimal idProfilRessource;

	@Column(name="id_provider")
	private BigDecimal idProvider;

	@Column(name="id_qualif")
	private BigDecimal idQualif;

	@Column(name="id_secteur")
	private BigDecimal idSecteur;

	@Column(name="id_service")
	private BigDecimal idService;

	@Column(name="id_soc")
	private BigDecimal idSoc;

	@Column(name="id_soc_source")
	private BigDecimal idSocSource;

	@Column(name="id_specialite")
	private BigDecimal idSpecialite;

	@Column(name="id_squalif")
	private BigDecimal idSqualif;

	@Column(name="id_type_diplome")
	private BigDecimal idTypeDiplome;

	@Column(name="id_type_pers")
	private BigDecimal idTypePers;

	@Column(name="id_type_source")
	private BigDecimal idTypeSource;

	@Column(name="id_unite")
	private BigDecimal idUnite;

	@Column(name="l_ecole")
	private String lEcole;

	@Column(name="lieu_naissance_pers")
	private String lieuNaissancePers;

	private Boolean masquer;

	@Column(name="mat_pers")
	private String matPers;

	@Column(name="mobilite_geo")
	private String mobiliteGeo;

	@Column(name="nb_annee_experience")
	private BigDecimal nbAnneeExperience;

	@Column(name="nb_mois_experience")
	private BigDecimal nbMoisExperience;

	@Column(name="nbr_annee_exp")
	private BigDecimal nbrAnneeExp;

	@Column(name="nbr_enfant")
	private BigDecimal nbrEnfant;

	@Column(name="nbr_tentative")
	private BigDecimal nbrTentative;

	@Column(name="nom_de_naissance_pers")
	private String nomDeNaissancePers;

	@Column(name="nom_pers")
	private String nomPers;

	@Column(name="num_nas")
	private String numNas;

	@Column(name="num_ramq")
	private String numRamq;

	@Column(name="num_ss")
	private String numSs;

	@Column(name="pays_naissance")
	private BigDecimal paysNaissance;

	@Column(name="permis_conduire")
	private String permisConduire;

	@Column(name="pers_actif")
	private Boolean persActif;

	private String pist;

	@Column(name="position_cv")
	private BigDecimal positionCv;

	@Column(name="prenom_pers")
	private String prenomPers;

	@Column(name="ref_user")
	private BigDecimal refUser;

	@Column(name="regime_ctr_actuel")
	private String regimeCtrActuel;

	@Column(name="salaire_actuel_brut")
	private BigDecimal salaireActuelBrut;

	@Column(name="salaire_partiel_brut")
	private BigDecimal salairePartielBrut;

	private byte[] signature;

	private Boolean staffer;

	@Column(name="stf_note")
	private String stfNote;

	@Column(name="synch_status")
	private String synchStatus;

	private BigDecimal taux;

	@Column(name="taux_activite_act")
	private BigDecimal tauxActiviteAct;

	@Column(name="taux_activite_staffing")
	private BigDecimal tauxActiviteStaffing;

	@Column(name="taux_frais")
	private BigDecimal tauxFrais;

	@Column(name="type_ressource")
	private String typeRessource;

	@Column(name="user_agent")
	private String userAgent;

	private BigDecimal version;

	public RefPersonnel() {
	}

	public long getIdPers() {
		return this.idPers;
	}

	public void setIdPers(long idPers) {
		this.idPers = idPers;
	}

	public BigDecimal getAnnualAmount() {
		return this.annualAmount;
	}

	public void setAnnualAmount(BigDecimal annualAmount) {
		this.annualAmount = annualAmount;
	}

	public String getBackgroundCheck() {
		return this.backgroundCheck;
	}

	public void setBackgroundCheck(String backgroundCheck) {
		this.backgroundCheck = backgroundCheck;
	}

	public String getCodeAnalytique() {
		return this.codeAnalytique;
	}

	public void setCodeAnalytique(String codeAnalytique) {
		this.codeAnalytique = codeAnalytique;
	}

	public String getCodePers() {
		return this.codePers;
	}

	public void setCodePers(String codePers) {
		this.codePers = codePers;
	}

	public BigDecimal getCoefficientCv() {
		return this.coefficientCv;
	}

	public void setCoefficientCv(BigDecimal coefficientCv) {
		this.coefficientCv = coefficientCv;
	}

	public String getCompteTiers() {
		return this.compteTiers;
	}

	public void setCompteTiers(String compteTiers) {
		this.compteTiers = compteTiers;
	}

	public BigDecimal getCreateur() {
		return this.createur;
	}

	public void setCreateur(BigDecimal createur) {
		this.createur = createur;
	}

	public BigDecimal getCreateurCollab() {
		return this.createurCollab;
	}

	public void setCreateurCollab(BigDecimal createurCollab) {
		this.createurCollab = createurCollab;
	}

	public Date getDateStc() {
		return this.dateStc;
	}

	public void setDateStc(Date dateStc) {
		this.dateStc = dateStc;
	}

	public String getDepartementNaissance() {
		return this.departementNaissance;
	}

	public void setDepartementNaissance(String departementNaissance) {
		this.departementNaissance = departementNaissance;
	}

	public String getDescSrc() {
		return this.descSrc;
	}

	public void setDescSrc(String descSrc) {
		this.descSrc = descSrc;
	}

	public String getDiscPers() {
		return this.discPers;
	}

	public void setDiscPers(String discPers) {
		this.discPers = discPers;
	}

	public String getDisponibilite() {
		return this.disponibilite;
	}

	public void setDisponibilite(String disponibilite) {
		this.disponibilite = disponibilite;
	}

	public String getDrugTest() {
		return this.drugTest;
	}

	public void setDrugTest(String drugTest) {
		this.drugTest = drugTest;
	}

	public Date getDtAnciennete() {
		return this.dtAnciennete;
	}

	public void setDtAnciennete(Date dtAnciennete) {
		this.dtAnciennete = dtAnciennete;
	}

	public Date getDtComplationBackground() {
		return this.dtComplationBackground;
	}

	public void setDtComplationBackground(Date dtComplationBackground) {
		this.dtComplationBackground = dtComplationBackground;
	}

	public Date getDtComplationDrug() {
		return this.dtComplationDrug;
	}

	public void setDtComplationDrug(Date dtComplationDrug) {
		this.dtComplationDrug = dtComplationDrug;
	}

	public Timestamp getDtCreate() {
		return this.dtCreate;
	}

	public void setDtCreate(Timestamp dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Date getDtDisponibilite() {
		return this.dtDisponibilite;
	}

	public void setDtDisponibilite(Date dtDisponibilite) {
		this.dtDisponibilite = dtDisponibilite;
	}

	public Date getDtEffetConge() {
		return this.dtEffetConge;
	}

	public void setDtEffetConge(Date dtEffetConge) {
		this.dtEffetConge = dtEffetConge;
	}

	public Date getDtEntreeEntite() {
		return this.dtEntreeEntite;
	}

	public void setDtEntreeEntite(Date dtEntreeEntite) {
		this.dtEntreeEntite = dtEntreeEntite;
	}

	public Timestamp getDtModif() {
		return this.dtModif;
	}

	public void setDtModif(Timestamp dtModif) {
		this.dtModif = dtModif;
	}

	public Date getDtNaissancePers() {
		return this.dtNaissancePers;
	}

	public void setDtNaissancePers(Date dtNaissancePers) {
		this.dtNaissancePers = dtNaissancePers;
	}

	public Date getDtObtentionDip() {
		return this.dtObtentionDip;
	}

	public void setDtObtentionDip(Date dtObtentionDip) {
		this.dtObtentionDip = dtObtentionDip;
	}

	public Timestamp getDtPremiereExperience() {
		return this.dtPremiereExperience;
	}

	public void setDtPremiereExperience(Timestamp dtPremiereExperience) {
		this.dtPremiereExperience = dtPremiereExperience;
	}

	public Date getDtVisiteMedicale() {
		return this.dtVisiteMedicale;
	}

	public void setDtVisiteMedicale(Date dtVisiteMedicale) {
		this.dtVisiteMedicale = dtVisiteMedicale;
	}

	public BigDecimal getEntiteJuridique() {
		return this.entiteJuridique;
	}

	public void setEntiteJuridique(BigDecimal entiteJuridique) {
		this.entiteJuridique = entiteJuridique;
	}

	public Boolean getEstForce() {
		return this.estForce;
	}

	public void setEstForce(Boolean estForce) {
		this.estForce = estForce;
	}

	public Boolean getExitMailsSent() {
		return this.exitMailsSent;
	}

	public void setExitMailsSent(Boolean exitMailsSent) {
		this.exitMailsSent = exitMailsSent;
	}

	public Boolean getHasCashAdvances() {
		return this.hasCashAdvances;
	}

	public void setHasCashAdvances(Boolean hasCashAdvances) {
		this.hasCashAdvances = hasCashAdvances;
	}

	public Boolean getHasPto() {
		return this.hasPto;
	}

	public void setHasPto(Boolean hasPto) {
		this.hasPto = hasPto;
	}

	public BigDecimal getIdBuManagerRh() {
		return this.idBuManagerRh;
	}

	public void setIdBuManagerRh(BigDecimal idBuManagerRh) {
		this.idBuManagerRh = idBuManagerRh;
	}

	public BigDecimal getIdCandDateDispo() {
		return this.idCandDateDispo;
	}

	public void setIdCandDateDispo(BigDecimal idCandDateDispo) {
		this.idCandDateDispo = idCandDateDispo;
	}

	public String getIdCandidatAts() {
		return this.idCandidatAts;
	}

	public void setIdCandidatAts(String idCandidatAts) {
		this.idCandidatAts = idCandidatAts;
	}

	public BigDecimal getIdCiv() {
		return this.idCiv;
	}

	public void setIdCiv(BigDecimal idCiv) {
		this.idCiv = idCiv;
	}

	public BigDecimal getIdCivilite() {
		return this.idCivilite;
	}

	public void setIdCivilite(BigDecimal idCivilite) {
		this.idCivilite = idCivilite;
	}

	public BigDecimal getIdCollabCategorie() {
		return this.idCollabCategorie;
	}

	public void setIdCollabCategorie(BigDecimal idCollabCategorie) {
		this.idCollabCategorie = idCollabCategorie;
	}

	public BigDecimal getIdCooptant() {
		return this.idCooptant;
	}

	public void setIdCooptant(BigDecimal idCooptant) {
		this.idCooptant = idCooptant;
	}

	public BigDecimal getIdEtab() {
		return this.idEtab;
	}

	public void setIdEtab(BigDecimal idEtab) {
		this.idEtab = idEtab;
	}

	public BigDecimal getIdFonction() {
		return this.idFonction;
	}

	public void setIdFonction(BigDecimal idFonction) {
		this.idFonction = idFonction;
	}

	public BigDecimal getIdFormation() {
		return this.idFormation;
	}

	public void setIdFormation(BigDecimal idFormation) {
		this.idFormation = idFormation;
	}

	public BigDecimal getIdInfoContrat() {
		return this.idInfoContrat;
	}

	public void setIdInfoContrat(BigDecimal idInfoContrat) {
		this.idInfoContrat = idInfoContrat;
	}

	public String getIdJobAts() {
		return this.idJobAts;
	}

	public void setIdJobAts(String idJobAts) {
		this.idJobAts = idJobAts;
	}

	public BigDecimal getIdManager() {
		return this.idManager;
	}

	public void setIdManager(BigDecimal idManager) {
		this.idManager = idManager;
	}

	public BigDecimal getIdNationalite() {
		return this.idNationalite;
	}

	public void setIdNationalite(BigDecimal idNationalite) {
		this.idNationalite = idNationalite;
	}

	public BigDecimal getIdOffre() {
		return this.idOffre;
	}

	public void setIdOffre(BigDecimal idOffre) {
		this.idOffre = idOffre;
	}

	public BigDecimal getIdPaysEcole() {
		return this.idPaysEcole;
	}

	public void setIdPaysEcole(BigDecimal idPaysEcole) {
		this.idPaysEcole = idPaysEcole;
	}

	public BigDecimal getIdPersAssurance() {
		return this.idPersAssurance;
	}

	public void setIdPersAssurance(BigDecimal idPersAssurance) {
		this.idPersAssurance = idPersAssurance;
	}

	public BigDecimal getIdPersSource() {
		return this.idPersSource;
	}

	public void setIdPersSource(BigDecimal idPersSource) {
		this.idPersSource = idPersSource;
	}

	public BigDecimal getIdProfil() {
		return this.idProfil;
	}

	public void setIdProfil(BigDecimal idProfil) {
		this.idProfil = idProfil;
	}

	public BigDecimal getIdProfilRessource() {
		return this.idProfilRessource;
	}

	public void setIdProfilRessource(BigDecimal idProfilRessource) {
		this.idProfilRessource = idProfilRessource;
	}

	public BigDecimal getIdProvider() {
		return this.idProvider;
	}

	public void setIdProvider(BigDecimal idProvider) {
		this.idProvider = idProvider;
	}

	public BigDecimal getIdQualif() {
		return this.idQualif;
	}

	public void setIdQualif(BigDecimal idQualif) {
		this.idQualif = idQualif;
	}

	public BigDecimal getIdSecteur() {
		return this.idSecteur;
	}

	public void setIdSecteur(BigDecimal idSecteur) {
		this.idSecteur = idSecteur;
	}

	public BigDecimal getIdService() {
		return this.idService;
	}

	public void setIdService(BigDecimal idService) {
		this.idService = idService;
	}

	public BigDecimal getIdSoc() {
		return this.idSoc;
	}

	public void setIdSoc(BigDecimal idSoc) {
		this.idSoc = idSoc;
	}

	public BigDecimal getIdSocSource() {
		return this.idSocSource;
	}

	public void setIdSocSource(BigDecimal idSocSource) {
		this.idSocSource = idSocSource;
	}

	public BigDecimal getIdSpecialite() {
		return this.idSpecialite;
	}

	public void setIdSpecialite(BigDecimal idSpecialite) {
		this.idSpecialite = idSpecialite;
	}

	public BigDecimal getIdSqualif() {
		return this.idSqualif;
	}

	public void setIdSqualif(BigDecimal idSqualif) {
		this.idSqualif = idSqualif;
	}

	public BigDecimal getIdTypeDiplome() {
		return this.idTypeDiplome;
	}

	public void setIdTypeDiplome(BigDecimal idTypeDiplome) {
		this.idTypeDiplome = idTypeDiplome;
	}

	public BigDecimal getIdTypePers() {
		return this.idTypePers;
	}

	public void setIdTypePers(BigDecimal idTypePers) {
		this.idTypePers = idTypePers;
	}

	public BigDecimal getIdTypeSource() {
		return this.idTypeSource;
	}

	public void setIdTypeSource(BigDecimal idTypeSource) {
		this.idTypeSource = idTypeSource;
	}

	public BigDecimal getIdUnite() {
		return this.idUnite;
	}

	public void setIdUnite(BigDecimal idUnite) {
		this.idUnite = idUnite;
	}

	public String getLEcole() {
		return this.lEcole;
	}

	public void setLEcole(String lEcole) {
		this.lEcole = lEcole;
	}

	public String getLieuNaissancePers() {
		return this.lieuNaissancePers;
	}

	public void setLieuNaissancePers(String lieuNaissancePers) {
		this.lieuNaissancePers = lieuNaissancePers;
	}

	public Boolean getMasquer() {
		return this.masquer;
	}

	public void setMasquer(Boolean masquer) {
		this.masquer = masquer;
	}

	public String getMatPers() {
		return this.matPers;
	}

	public void setMatPers(String matPers) {
		this.matPers = matPers;
	}

	public String getMobiliteGeo() {
		return this.mobiliteGeo;
	}

	public void setMobiliteGeo(String mobiliteGeo) {
		this.mobiliteGeo = mobiliteGeo;
	}

	public BigDecimal getNbAnneeExperience() {
		return this.nbAnneeExperience;
	}

	public void setNbAnneeExperience(BigDecimal nbAnneeExperience) {
		this.nbAnneeExperience = nbAnneeExperience;
	}

	public BigDecimal getNbMoisExperience() {
		return this.nbMoisExperience;
	}

	public void setNbMoisExperience(BigDecimal nbMoisExperience) {
		this.nbMoisExperience = nbMoisExperience;
	}

	public BigDecimal getNbrAnneeExp() {
		return this.nbrAnneeExp;
	}

	public void setNbrAnneeExp(BigDecimal nbrAnneeExp) {
		this.nbrAnneeExp = nbrAnneeExp;
	}

	public BigDecimal getNbrEnfant() {
		return this.nbrEnfant;
	}

	public void setNbrEnfant(BigDecimal nbrEnfant) {
		this.nbrEnfant = nbrEnfant;
	}

	public BigDecimal getNbrTentative() {
		return this.nbrTentative;
	}

	public void setNbrTentative(BigDecimal nbrTentative) {
		this.nbrTentative = nbrTentative;
	}

	public String getNomDeNaissancePers() {
		return this.nomDeNaissancePers;
	}

	public void setNomDeNaissancePers(String nomDeNaissancePers) {
		this.nomDeNaissancePers = nomDeNaissancePers;
	}

	public String getNomPers() {
		return this.nomPers;
	}

	public void setNomPers(String nomPers) {
		this.nomPers = nomPers;
	}

	public String getNumNas() {
		return this.numNas;
	}

	public void setNumNas(String numNas) {
		this.numNas = numNas;
	}

	public String getNumRamq() {
		return this.numRamq;
	}

	public void setNumRamq(String numRamq) {
		this.numRamq = numRamq;
	}

	public String getNumSs() {
		return this.numSs;
	}

	public void setNumSs(String numSs) {
		this.numSs = numSs;
	}

	public BigDecimal getPaysNaissance() {
		return this.paysNaissance;
	}

	public void setPaysNaissance(BigDecimal paysNaissance) {
		this.paysNaissance = paysNaissance;
	}

	public String getPermisConduire() {
		return this.permisConduire;
	}

	public void setPermisConduire(String permisConduire) {
		this.permisConduire = permisConduire;
	}

	public Boolean getPersActif() {
		return this.persActif;
	}

	public void setPersActif(Boolean persActif) {
		this.persActif = persActif;
	}

	public String getPist() {
		return this.pist;
	}

	public void setPist(String pist) {
		this.pist = pist;
	}

	public BigDecimal getPositionCv() {
		return this.positionCv;
	}

	public void setPositionCv(BigDecimal positionCv) {
		this.positionCv = positionCv;
	}

	public String getPrenomPers() {
		return this.prenomPers;
	}

	public void setPrenomPers(String prenomPers) {
		this.prenomPers = prenomPers;
	}

	public BigDecimal getRefUser() {
		return this.refUser;
	}

	public void setRefUser(BigDecimal refUser) {
		this.refUser = refUser;
	}

	public String getRegimeCtrActuel() {
		return this.regimeCtrActuel;
	}

	public void setRegimeCtrActuel(String regimeCtrActuel) {
		this.regimeCtrActuel = regimeCtrActuel;
	}

	public BigDecimal getSalaireActuelBrut() {
		return this.salaireActuelBrut;
	}

	public void setSalaireActuelBrut(BigDecimal salaireActuelBrut) {
		this.salaireActuelBrut = salaireActuelBrut;
	}

	public BigDecimal getSalairePartielBrut() {
		return this.salairePartielBrut;
	}

	public void setSalairePartielBrut(BigDecimal salairePartielBrut) {
		this.salairePartielBrut = salairePartielBrut;
	}

	public byte[] getSignature() {
		return this.signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	public Boolean getStaffer() {
		return this.staffer;
	}

	public void setStaffer(Boolean staffer) {
		this.staffer = staffer;
	}

	public String getStfNote() {
		return this.stfNote;
	}

	public void setStfNote(String stfNote) {
		this.stfNote = stfNote;
	}

	public String getSynchStatus() {
		return this.synchStatus;
	}

	public void setSynchStatus(String synchStatus) {
		this.synchStatus = synchStatus;
	}

	public BigDecimal getTaux() {
		return this.taux;
	}

	public void setTaux(BigDecimal taux) {
		this.taux = taux;
	}

	public BigDecimal getTauxActiviteAct() {
		return this.tauxActiviteAct;
	}

	public void setTauxActiviteAct(BigDecimal tauxActiviteAct) {
		this.tauxActiviteAct = tauxActiviteAct;
	}

	public BigDecimal getTauxActiviteStaffing() {
		return this.tauxActiviteStaffing;
	}

	public void setTauxActiviteStaffing(BigDecimal tauxActiviteStaffing) {
		this.tauxActiviteStaffing = tauxActiviteStaffing;
	}

	public BigDecimal getTauxFrais() {
		return this.tauxFrais;
	}

	public void setTauxFrais(BigDecimal tauxFrais) {
		this.tauxFrais = tauxFrais;
	}

	public String getTypeRessource() {
		return this.typeRessource;
	}

	public void setTypeRessource(String typeRessource) {
		this.typeRessource = typeRessource;
	}

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public BigDecimal getVersion() {
		return this.version;
	}

	public void setVersion(BigDecimal version) {
		this.version = version;
	}

}