package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the prm_categ_crdn database table.
 * 
 */
@Entity
@Table(name="prm_categ_crdn")
@NamedQuery(name="PrmCategCrdn.findAll", query="SELECT p FROM PrmCategCrdn p")
public class PrmCategCrdn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_categ")
	private long idCateg;

	@Column(name="abrv_categ")
	private String abrvCateg;

	@Column(name="desc_src")
	private String descSrc;

	@Column(name="dt_create")
	private Timestamp dtCreate;

	@Column(name="dt_modif")
	private Timestamp dtModif;

	@Column(name="l_categorie_crdn")
	private String lCategorieCrdn;

	@Column(name="ordre_categ")
	private BigDecimal ordreCateg;

	@Column(name="ref_user")
	private BigDecimal refUser;

	@Column(name="user_agent")
	private String userAgent;

	private BigDecimal version;

	public PrmCategCrdn() {
	}

	public long getIdCateg() {
		return this.idCateg;
	}

	public void setIdCateg(long idCateg) {
		this.idCateg = idCateg;
	}

	public String getAbrvCateg() {
		return this.abrvCateg;
	}

	public void setAbrvCateg(String abrvCateg) {
		this.abrvCateg = abrvCateg;
	}

	public String getDescSrc() {
		return this.descSrc;
	}

	public void setDescSrc(String descSrc) {
		this.descSrc = descSrc;
	}

	public Timestamp getDtCreate() {
		return this.dtCreate;
	}

	public void setDtCreate(Timestamp dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Timestamp getDtModif() {
		return this.dtModif;
	}

	public void setDtModif(Timestamp dtModif) {
		this.dtModif = dtModif;
	}

	public String getLCategorieCrdn() {
		return this.lCategorieCrdn;
	}

	public void setLCategorieCrdn(String lCategorieCrdn) {
		this.lCategorieCrdn = lCategorieCrdn;
	}

	public BigDecimal getOrdreCateg() {
		return this.ordreCateg;
	}

	public void setOrdreCateg(BigDecimal ordreCateg) {
		this.ordreCateg = ordreCateg;
	}

	public BigDecimal getRefUser() {
		return this.refUser;
	}

	public void setRefUser(BigDecimal refUser) {
		this.refUser = refUser;
	}

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public BigDecimal getVersion() {
		return this.version;
	}

	public void setVersion(BigDecimal version) {
		this.version = version;
	}

}