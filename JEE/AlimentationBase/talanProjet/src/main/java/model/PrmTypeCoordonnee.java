package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the prm_type_coordonnees database table.
 * 
 */
@Entity
@Table(name="prm_type_coordonnees")
@NamedQuery(name="PrmTypeCoordonnee.findAll", query="SELECT p FROM PrmTypeCoordonnee p")
public class PrmTypeCoordonnee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_type_crdn")
	private long idTypeCrdn;

	@Column(name="abrv_type_cdr")
	private String abrvTypeCdr;

	private BigDecimal createur;

	@Column(name="desc_src")
	private String descSrc;

	@Column(name="dt_create")
	private Timestamp dtCreate;

	@Column(name="dt_modif")
	private Timestamp dtModif;

	@Column(name="id_categ")
	private BigDecimal idCateg;

	@Column(name="l_type_coordonnees")
	private String lTypeCoordonnees;

	@Column(name="ordre_type_crd")
	private BigDecimal ordreTypeCrd;

	@Column(name="ref_user")
	private BigDecimal refUser;

	@Column(name="user_agent")
	private String userAgent;

	private BigDecimal version;

	public PrmTypeCoordonnee() {
	}

	public long getIdTypeCrdn() {
		return this.idTypeCrdn;
	}

	public void setIdTypeCrdn(long idTypeCrdn) {
		this.idTypeCrdn = idTypeCrdn;
	}

	public String getAbrvTypeCdr() {
		return this.abrvTypeCdr;
	}

	public void setAbrvTypeCdr(String abrvTypeCdr) {
		this.abrvTypeCdr = abrvTypeCdr;
	}

	public BigDecimal getCreateur() {
		return this.createur;
	}

	public void setCreateur(BigDecimal createur) {
		this.createur = createur;
	}

	public String getDescSrc() {
		return this.descSrc;
	}

	public void setDescSrc(String descSrc) {
		this.descSrc = descSrc;
	}

	public Timestamp getDtCreate() {
		return this.dtCreate;
	}

	public void setDtCreate(Timestamp dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Timestamp getDtModif() {
		return this.dtModif;
	}

	public void setDtModif(Timestamp dtModif) {
		this.dtModif = dtModif;
	}

	public BigDecimal getIdCateg() {
		return this.idCateg;
	}

	public void setIdCateg(BigDecimal idCateg) {
		this.idCateg = idCateg;
	}

	public String getLTypeCoordonnees() {
		return this.lTypeCoordonnees;
	}

	public void setLTypeCoordonnees(String lTypeCoordonnees) {
		this.lTypeCoordonnees = lTypeCoordonnees;
	}

	public BigDecimal getOrdreTypeCrd() {
		return this.ordreTypeCrd;
	}

	public void setOrdreTypeCrd(BigDecimal ordreTypeCrd) {
		this.ordreTypeCrd = ordreTypeCrd;
	}

	public BigDecimal getRefUser() {
		return this.refUser;
	}

	public void setRefUser(BigDecimal refUser) {
		this.refUser = refUser;
	}

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public BigDecimal getVersion() {
		return this.version;
	}

	public void setVersion(BigDecimal version) {
		this.version = version;
	}

}