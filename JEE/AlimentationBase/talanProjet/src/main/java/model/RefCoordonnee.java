package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ref_coordonnees database table.
 * 
 */
@Entity
@Table(name="ref_coordonnees")
@NamedQuery(name="RefCoordonnee.findAll", query="SELECT r FROM RefCoordonnee r")
public class RefCoordonnee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_crdn")
	private long idCrdn;

	private Boolean autorise;

	private BigDecimal createur;

	@Column(name="desc_src")
	private String descSrc;

	private String discriminateur;

	@Column(name="dt_create")
	private Timestamp dtCreate;

	@Column(name="dt_modif")
	private Timestamp dtModif;

	@Column(name="id_categ")
	private BigDecimal idCateg;

	@Column(name="id_pays")
	private BigDecimal idPays;

	@Column(name="id_pers")
	private BigDecimal idPers;

	@Column(name="id_soc")
	private BigDecimal idSoc;

	@Column(name="id_type_crdn")
	private BigDecimal idTypeCrdn;

	@Column(name="is_default")
	private Boolean isDefault;

	@Column(name="ref_user")
	private BigDecimal refUser;

	@Column(name="user_agent")
	private String userAgent;

	@Column(name="val_coordonnees")
	private String valCoordonnees;

	private BigDecimal version;

	public RefCoordonnee() {
	}

	public long getIdCrdn() {
		return this.idCrdn;
	}

	public void setIdCrdn(long idCrdn) {
		this.idCrdn = idCrdn;
	}

	public Boolean getAutorise() {
		return this.autorise;
	}

	public void setAutorise(Boolean autorise) {
		this.autorise = autorise;
	}

	public BigDecimal getCreateur() {
		return this.createur;
	}

	public void setCreateur(BigDecimal createur) {
		this.createur = createur;
	}

	public String getDescSrc() {
		return this.descSrc;
	}

	public void setDescSrc(String descSrc) {
		this.descSrc = descSrc;
	}

	public String getDiscriminateur() {
		return this.discriminateur;
	}

	public void setDiscriminateur(String discriminateur) {
		this.discriminateur = discriminateur;
	}

	public Timestamp getDtCreate() {
		return this.dtCreate;
	}

	public void setDtCreate(Timestamp dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Timestamp getDtModif() {
		return this.dtModif;
	}

	public void setDtModif(Timestamp dtModif) {
		this.dtModif = dtModif;
	}

	public BigDecimal getIdCateg() {
		return this.idCateg;
	}

	public void setIdCateg(BigDecimal idCateg) {
		this.idCateg = idCateg;
	}

	public BigDecimal getIdPays() {
		return this.idPays;
	}

	public void setIdPays(BigDecimal idPays) {
		this.idPays = idPays;
	}

	public BigDecimal getIdPers() {
		return this.idPers;
	}

	public void setIdPers(BigDecimal idPers) {
		this.idPers = idPers;
	}

	public BigDecimal getIdSoc() {
		return this.idSoc;
	}

	public void setIdSoc(BigDecimal idSoc) {
		this.idSoc = idSoc;
	}

	public BigDecimal getIdTypeCrdn() {
		return this.idTypeCrdn;
	}

	public void setIdTypeCrdn(BigDecimal idTypeCrdn) {
		this.idTypeCrdn = idTypeCrdn;
	}

	public Boolean getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public BigDecimal getRefUser() {
		return this.refUser;
	}

	public void setRefUser(BigDecimal refUser) {
		this.refUser = refUser;
	}

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getValCoordonnees() {
		return this.valCoordonnees;
	}

	public void setValCoordonnees(String valCoordonnees) {
		this.valCoordonnees = valCoordonnees;
	}

	public BigDecimal getVersion() {
		return this.version;
	}

	public void setVersion(BigDecimal version) {
		this.version = version;
	}

}